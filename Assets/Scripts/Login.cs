﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using UnityEngine;
using UnityEngine.SceneManagement;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using Firebase.Auth;
using System;

public class Login : MonoBehaviour
{

    private Firebase.Auth.FirebaseAuth auth;
    private Firebase.Auth.FirebaseUser user;
    DatabaseReference reference;
    string userID = "";
    string displayName = "";
    string userEmail = "";
    void Start()
    {
        // Iniciar la configuración de Play Games.
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
         .EnableSavedGames()
         .RequestServerAuthCode(false /*forceRefresh*/)
         .RequestEmail()
         .Build();
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.Activate();
        Debug.LogFormat("Play Games Configuration initialized");
        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        SignInWithPlayGames();
    }

    private string saveDataToFirebase(string userId, string userName, string userEmail) // Guardar la informacion en la BBDD
    {
        reference.Child("Users").Child(userId.ToString()).Child("DisplayName").SetValueAsync(userName);
        reference.Child("Users").Child(userId.ToString()).Child("UserEmail").SetValueAsync(userEmail);
        return "Save data to firebase Done.";
    }
    private void SignInWithPlayGames()
    {
        // Inicia sesión en Google Play Games
        PlayGamesPlatform.Instance.Authenticate((bool success) =>
        {
            if (!success)
            {

                Debug.LogError("Failed to Sign into Play Games Services.");
                return;
            }

            //Codigo de google play games
            string authCode = PlayGamesPlatform.Instance.GetServerAuthCode();

            if (string.IsNullOrEmpty(authCode))
            {
                Debug.LogError("Signed into Play Games Services but failed to get the server auth code.");
                return;
            }
            Debug.LogFormat("Auth code is: {0}", authCode);

            // Crea un token para autenticar con firebase
            Firebase.Auth.Credential credential = Firebase.Auth.PlayGamesAuthProvider.GetCredential(authCode);

            // Inicia sesión en firebase utilizando el token
            auth.SignInWithCredentialAsync(credential).ContinueWith(task =>
            {
                if (task.IsCanceled)
                {
                    Debug.LogError("Play games login was canceled.");
                    return;
                }
                if (task.IsFaulted)
                {
                    Debug.LogError("Play games login encountered an error: " + task.Exception);
                    return;
                }

                Firebase.Auth.FirebaseUser newUser = task.Result;
                displayName = newUser.DisplayName;
                userID = newUser.UserId;
                userEmail = ((PlayGamesLocalUser)Social.localUser).Email;


                Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(firebaseTask =>
                {
                    var dependencyStatus = firebaseTask.Result;
                    if (dependencyStatus == Firebase.DependencyStatus.Available)
                    {
                        // Referencia  ala BBDD.
                        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://igotu-91d1a.firebaseio.com/");

                        // Instanciar firebase real time database.
                        reference = FirebaseDatabase.DefaultInstance.RootReference;

                        //_________________________

                        string DebugMsg = saveDataToFirebase(userID, displayName, userEmail);
                        Debug.Log(DebugMsg);
                        LoadNextScene();
                    }
                    else
                    {
                        UnityEngine.Debug.LogError(System.String.Format(
                          "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                        // Firebase Unity SDK is not safe to use here.
                    }
                });


            });
        });
    }

    private void LoadNextScene()
    {
        SceneManager.LoadScene(1);
    }
}
