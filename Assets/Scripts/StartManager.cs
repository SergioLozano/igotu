﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Auth;
using UnityEngine.UI;
using TMPro;

public class StartManager : MonoBehaviour
{
    private Firebase.Auth.FirebaseAuth auth;
    private Firebase.Auth.FirebaseUser user;

    public TextMeshProUGUI tUserID;
    public TextMeshProUGUI tDisplayName;
    public TextMeshProUGUI tEmail;

    string userID = "";
    string displayName = "";
    string userEmail = "";
    // Start is called before the first frame update
    void Start()
    {
        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        user = auth.CurrentUser;

        if (user != null)
        {
            
            displayName = user.DisplayName;
            userEmail = user.Email;
            userID = user.UserId;
            Debug.Log("User ready");
        }

        updateUserInfo();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void updateUserInfo()
    {
        tUserID.SetText(userID);
        tEmail.SetText(userEmail);
        tDisplayName.SetText(displayName);
    }

    public void LogOut()
    {
        auth.SignOut();
    }
}
