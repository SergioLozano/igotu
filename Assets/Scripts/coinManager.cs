﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class coinManager : MonoBehaviour
{
    private int playerCoins;
    public TextMeshProUGUI tCoins;

    private void Awake()
    {
        //awake
    }
    private void Start()
    {
        //start
        playerCoins = PlayerPrefs.GetInt("playerCoins", 0);// replace -> get firebase user coins !
        updateCoinsUI();
    }

    private void updateCoinsUI()
    {
        tCoins.SetText("x " + playerCoins.ToString());
    }
}
