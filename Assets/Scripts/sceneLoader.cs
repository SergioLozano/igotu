﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class sceneLoader : MonoBehaviour
{
    public void JumpToScene(string level)
    {
        SceneManager.LoadScene(level);
    }
}
