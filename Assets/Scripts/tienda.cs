﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Monetization;


//https://www.youtube.com/watch?v=v7A3pvymquY
public class tienda : MonoBehaviour
{
    private  string store_id = "3312406";
    private string video_ad = "rewardedVideo";
    public void JumpToScene(string level)
    {
        SceneManager.LoadScene(level);
    }
    public void playAdd() {
        Monetization.Initialize(store_id,true);
        if (Monetization.IsReady(video_ad))
        {
            ShowAdPlacementContent ad = null;
            ad = Monetization.GetPlacementContent(video_ad) as ShowAdPlacementContent;

            if (ad != null) {
                ad.Show();
            }
        }
    }
}
